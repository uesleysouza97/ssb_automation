package App;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDatabase {

    private Connection connection = null;
    protected String local, port, database, user, password, driver;

    public ConnectionDatabase(String driver, String local, String port,
            String database, String user, String password) {

        this.local = local;
        this.driver = driver;
        this.database = database;
        this.port = port;
        this.user = user;
        this.password = password;
        
    }

    public void connect() {

        try {
            connection = DriverManager.getConnection(
                    "jdbc:"+ driver +"://" + local + ":" + port + "/"
                    + database, user, password);

        } catch (SQLException e) {

            System.out.println("A conexão com o banco de dados falhou! Verifique a saída do Console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("Banco de dados conectado.");
        } else {
            System.out.println("A conexão com o banco de dados falhou, "
                    + "verifique os dados do arquivo de configuração");
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public String getDatabase() {
        return database;
    }
    

}

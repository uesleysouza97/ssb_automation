package App;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import Queries.Query1;
import Queries.Query2;
import Queries.Query3;
import QueriesDAO.Query1DAO;
import QueriesDAO.Query2DAO;
import QueriesDAO.Query3DAO;
import Results.ResultQuery1;
import Results.ResultQuery2;
import Results.ResultQuery3;
import ResultsDAO.ResultQuery1DAO;
import ResultsDAO.ResultQuery2DAO;
import ResultsDAO.ResultQuery3DAO;

public class App {

    static ConnectionDatabase connSSB;
    static ConnectionDatabase connSSBResults;

    public static void main(String[] args) {
        System.out.println("*** SSB Automation ***");
        startConnection();
        menu();

    }

    public static void startConnection() {
        try {
            BufferedReader br = new BufferedReader(new FileReader("./config.ini"));
            String[] data = new String[7];
            int i = 0;
            while (br.ready()) {
                String line = br.readLine();
                data[i] = (line.split("="))[1];
                i++;
            }
            br.close();
            connSSB = new ConnectionDatabase(data[0], data[1],
                    data[2], data[3], data[4], data[5]);
            connSSBResults = new ConnectionDatabase(data[0], data[1],
                    data[2], data[6], data[4], data[5]);
            connSSB.connect();
            connSSBResults.connect();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public static void menu() {

        int num = -1;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Informe o código da transação: \n1 - Executar consultas\n"
                    + "2 - Sujar banco de dados\n3 - Análises estatisticas\n0 - Sair");
            num = scanner.nextInt();
            switch (num) {
                case 1:
                    menuConsultas();
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 0:
                    System.out.println("Good Bye!");
                    break;
            }
        } while (num != 0);

    }

    public static void menuConsultas() {

        int numQuery = -1;
        Scanner scanner = new Scanner(System.in);
        do {
            System.out.println("Informe o código da consulta (0 -> voltar): ");
            numQuery = scanner.nextInt();
            switch (numQuery) {
                case 1:
                    executeQuery1();
                    break;
                case 2:
                    executeQuery2();
                    break;
                case 3:
                    executeQuery3();
                    break;
                case 4:
                    break;
                case 0:
                    System.out.println("Good Bye!");
                    break;
            }
        } while (numQuery != 0);
    }

    public static void executeQuery1() {

        Random random = new Random();
        Query1 query = new Query1();
        Integer year, discount, quantity;
        year = random.nextInt(5) + 1993;
        discount = random.nextInt(8) + 2;
        quantity = random.nextInt(2) + 24;
        query.setYear(year);
        query.setDiscount(discount);
        query.setQuantity(quantity);
        Query1DAO dao = new Query1DAO(connSSB.getConnection(), query);
        ResultQuery1 result = dao.execute();
        ResultQuery1DAO daoResult = new ResultQuery1DAO(connSSBResults.getConnection(),
                result, connSSB.getDatabase());
        daoResult.save();
        System.out.println("Revenue = " + result.getRevenue());
        System.out.println("Parameters: ");
        System.out.println("Year = " + year);
        System.out.println("Discount = " + discount);
        System.out.println("Quantity = " + quantity);

    }

    public static void executeQuery2() {

        Query2 query = new Query2();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o número da subconsulta: ");
        query.setSubQuery(scanner.nextInt());

        if (query.getSubQuery() == 1) {
            query.setCategory("MFGR#12");
            query.setRegion("AMERICA");
        }
        if (query.getSubQuery() == 2) {
            query.setBrand1("MFGR#2221'");
            query.setBrand1_1("MFGR#2228");
            query.setRegion("ASIA");
        }
        if (query.getSubQuery() == 3) {
            query.setCategory("MFGR#12");
            query.setBrand1("MFGR#2339");
            query.setRegion("EUROPE");
        }

        Query2DAO dao = new Query2DAO(connSSB.getConnection(), query);
        dao.execute();
        ResultQuery2 result = dao.getResult();
        ResultQuery2DAO resultDAO = new ResultQuery2DAO(connSSBResults.getConnection(),
                result, connSSB.getDatabase());
        resultDAO.save();
        System.out.println("\n\n***Results***\n\nRevenue         Year        Brand");
        for (int i = 0; i < result.getRevenue().size(); i++) {
            System.out.println(result.getRevenue().get(i) + "      "
                    + result.getYear().get(i) + "       " + result.getBrand().get(i));
        }
    }

    public static void executeQuery3() {

        int numSubQuery;
        Query3 query = null;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o número da subconsulta: ");
        numSubQuery = scanner.nextInt();

        if (numSubQuery == 1) {
            query = new Query3("ASIA", "ASIA", "1992", "1997",
                    null, null, null, null, null, null, null);
        }
        if (numSubQuery == 2) {
            query = new Query3(null, null, "1992", "1997",
                    "UNITED STATES", "UNITED STATES", null,
                    null, null, null, null);
        }
        if (numSubQuery == 3) {
            query = new Query3(null, null, "1992", "1997", null,
                    null, "UNITED KI1", "UNITED KI5", "UNITED KI1",
                    "UNITED KI5", null);
        }
        if (numSubQuery == 4) {
            query = new Query3(null, null, null, null, null, null,
                    "UNITED KI1", "UNITED KI5", "UNITED KI1",
                    "UNITED KI5", "Dec1997");
        }
        if (query != null) {
            query.setSubQuery(numSubQuery);
        }
        Query3DAO dao = new Query3DAO(connSSB.getConnection(), query);
        dao.execute();
        ResultQuery3 result = dao.getResult();
        ResultQuery3DAO resultDAO = new ResultQuery3DAO(connSSBResults.getConnection(), result, connSSB.getDatabase());
        resultDAO.save();
    }

}

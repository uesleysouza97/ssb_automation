package ResultsDAO;

import Results.ResultQuery2;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResultQuery2DAO {

    Connection conn;
    ResultQuery2 result;
    String database;
    PreparedStatement preparedStatement = null;

    public ResultQuery2DAO(Connection conn, ResultQuery2 result, String database) {
        this.conn = conn;
        this.result = result;
        this.database = database;
    }

    public void save() {
        for (int i = 0; i < result.getRevenue().size(); i++) {
            String sql = "insert into query2Results (revenue, year, brand1,"
                    + " database_name, id_subquery) values("
                    + result.getRevenue().get(i).toString() + "," + result.getYear().get(i) + ",'"
                    + result.getBrand().get(i) + "','"
                    + database + "',"+result.getSubQuery()+")";
            System.out.println("QUERY: " + sql);
            try {
                preparedStatement = conn.prepareStatement(sql);
                preparedStatement.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(ResultQuery1DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

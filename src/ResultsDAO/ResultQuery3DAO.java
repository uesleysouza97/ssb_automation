package ResultsDAO;

import Results.ResultQuery3;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResultQuery3DAO {

    Connection conn;
    ResultQuery3 result;
    String database;
    PreparedStatement preparedStatement = null;

    public ResultQuery3DAO(Connection conn, ResultQuery3 result, String database) {
        this.conn = conn;
        this.result = result;
        this.database = database;
    }

    public void save() {
        System.out.println("Inserindo registos em: " + database+"...");
        if (result.getSubQuery() == 1) {
            for (int i = 0; i < result.getRevenue().size(); i++) {
                String sql = "insert into query3Results (revenue, year, customerNation,"
                        + " supplierNation, id_subquery, database_name) values ("
                        + result.getRevenue().get(i).toString() + "," + result.getYear().get(i) + ",'"
                        + result.getCustomerNation().get(i) + "','"
                        + result.getSupplierNation().get(i) + "',"
                        + result.getSubQuery() + ",'" + database + "')";
                
                try {
                    preparedStatement = conn.prepareStatement(sql);
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    Logger.getLogger(ResultQuery1DAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            for (int i = 0; i < result.getRevenue().size(); i++) {
                String sql = "insert into query3Results (revenue, year, customerCity,"
                        + " supplierCity, id_subquery, database_name) values ("
                        + result.getRevenue().get(i) + "," + result.getYear().get(i) + ",'"
                        + result.getCustomerCity().get(i) + "','"
                        + result.getSupplierCity().get(i) + "',"
                        + result.getSubQuery() + ",'" + database + "')";
                try {
                    preparedStatement = conn.prepareStatement(sql);
                    preparedStatement.executeUpdate();
                } catch (SQLException ex) {
                    Logger.getLogger(ResultQuery1DAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}

package ResultsDAO;

import Results.ResultQuery1;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ResultQuery1DAO {

    Connection conn;
    ResultQuery1 result;
    String database;
    PreparedStatement preparedStatement = null;

    public ResultQuery1DAO(Connection conn, ResultQuery1 result, String database) {
        this.conn = conn;
        this.result = result;
        this.database = database;
    }

    public void save() {
        String sql = "insert into query1Results (revenue, parameter_year, "
                + "parameter_discount, parameter_quantity, database_name)"
                + "values("
                + result.getRevenue().toString() + ","+result.getQuery().getYear() + ","
                + result.getQuery().getDiscount().toString() + ","
                + result.getQuery().getQuantity().toString() + ", '"
                + database + "')";
        System.out.println("QUERY: " + sql);
        try {
            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ResultQuery1DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }

    public ResultQuery1 getResult() {
        return result;
    }

    public void setResult(ResultQuery1 result) {
        this.result = result;
    }
    
}

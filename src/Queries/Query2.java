package Queries;

public class Query2 {
    
    private String category, region, brand1, brand1_1;
    private int subQuery;

    public Query2(String category, String region, String brand1, String brand1_1, int subQuery) {
        this.category = category;
        this.region = region;
        this.brand1 = brand1;
        this.brand1_1 = brand1_1;
        this.subQuery = subQuery;
    }

    public Query2() {  }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getBrand1() {
        return brand1;
    }

    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    public int getSubQuery() {
        return subQuery;
    }

    public void setSubQuery(int subQuery) {
        this.subQuery = subQuery;
    }

    public String getBrand1_1() {
        return brand1_1;
    }

    public void setBrand1_1(String brand1_1) {
        this.brand1_1 = brand1_1;
    }
    
}
package Queries;

public class Query1 {

    private Integer year, discount, quantity;

    public Query1(Integer year, Integer discount, Integer quantity) {
        this.year = year;
        this.discount = discount;
        this.quantity = quantity;
    }

    public Query1() {
    }
    
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    
}

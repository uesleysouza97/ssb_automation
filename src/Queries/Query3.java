package Queries;

public class Query3 {
    
    private String cRegion, sRegion, year, year_1, cNation, 
            sNation, cCity, cCity_1, sCity, sCity_1, yearMonth;
    private int subQuery;
    
    public Query3(String cRegion, String sRegion, String year, 
            String year_1, String cNation, String sNation, 
            String cCity, String cCity_1, String sCity,
            String sCity_1, String yearMonth) {
        this.cRegion = cRegion;
        this.sRegion = sRegion;
        this.year = year;
        this.year_1 = year_1;
        this.cNation = cNation;
        this.sNation = sNation;
        this.cCity = cCity;
        this.cCity_1 = cCity_1;
        this.sCity = sCity;
        this.sCity_1 = sCity_1;
        this.yearMonth = yearMonth;
    }

    public Query3() { }

    public String getcRegion() {
        return cRegion;
    }

    public void setcRegion(String cRegion) {
        this.cRegion = cRegion;
    }

    public String getsRegion() {
        return sRegion;
    }

    public void setsRegion(String sRegion) {
        this.sRegion = sRegion;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getcNation() {
        return cNation;
    }

    public void setcNation(String cNation) {
        this.cNation = cNation;
    }

    public String getsNation() {
        return sNation;
    }

    public void setsNation(String sNation) {
        this.sNation = sNation;
    }

    public String getcCity() {
        return cCity;
    }

    public void setcCity(String cCity) {
        this.cCity = cCity;
    }

    public int getSubQuery() {
        return subQuery;
    }

    public void setSubQuery(int subQuery) {
        this.subQuery = subQuery;
    }

    public String getYear_1() {
        return year_1;
    }

    public void setYear_1(String year_1) {
        this.year_1 = year_1;
    }

    public String getcCity_1() {
        return cCity_1;
    }

    public void setcCity_1(String cCity_1) {
        this.cCity_1 = cCity_1;
    }

    public String getsCity() {
        return sCity;
    }

    public void setsCity(String sCity) {
        this.sCity = sCity;
    }

    public String getsCity_1() {
        return sCity_1;
    }

    public void setsCity_1(String sCity_1) {
        this.sCity_1 = sCity_1;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }
    
}

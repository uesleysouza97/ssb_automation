package Results;

import Queries.Query1;

public class ResultQuery1 {
    
    private final Double revenue;
    public Query1 query;

    public ResultQuery1(Double revenue, Query1 query) {
        this.revenue = revenue;
        this.query = query;
    }

    public Double getRevenue() {
        return revenue;
    }

    public Query1 getQuery() {
        return query;
    }
    
}

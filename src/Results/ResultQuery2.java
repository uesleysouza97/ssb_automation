package Results;

import java.util.List;

public class ResultQuery2 {
    
    List<Double> revenue;
    List<String> year, brand;
    int subQuery;

    public ResultQuery2(List<Double> revenue, List<String> year, List<String> brand, int subQuery) {
        this.revenue = revenue;
        this.year = year;
        this.brand = brand;
        this.subQuery = subQuery;
    }

    public List<Double> getRevenue() {
        return revenue;
    }

    public List<String> getYear() {
        return year;
    }

    public List<String> getBrand() {
        return brand;
    }

    public int getSubQuery() {
        return subQuery;
    }
    
}

package Results;

import java.util.List;

public class ResultQuery3 {
    
    private List<Double> revenue;
    private List<String> customerNation, supplierNation, customerCity, supplierCity, year;
    int subQuery;

    public ResultQuery3(List<Double> revenue, List<String> customerNation, 
            List<String> supplierNation, List<String> customerCity, 
            List<String> supplierCity, List<String> year, int subQuery) {
        this.revenue = revenue;
        this.customerNation = customerNation;
        this.supplierNation = supplierNation;
        this.customerCity = customerCity;
        this.supplierCity = supplierCity;
        this.year = year;
        this.subQuery = subQuery;
    }

    public List<Double> getRevenue() {
        return revenue;
    }

    public void setRevenue(List<Double> revenue) {
        this.revenue = revenue;
    }

    public List<String> getCustomerNation() {
        return customerNation;
    }

    public void setCustomerNation(List<String> customerNation) {
        this.customerNation = customerNation;
    }

    public List<String> getSupplierNation() {
        return supplierNation;
    }

    public void setSupplierNation(List<String> supplierNation) {
        this.supplierNation = supplierNation;
    }

    public List<String> getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(List<String> customerCity) {
        this.customerCity = customerCity;
    }

    public List<String> getSupplierCity() {
        return supplierCity;
    }

    public void setSupplierCity(List<String> supplierCity) {
        this.supplierCity = supplierCity;
    }

    public List<String> getYear() {
        return year;
    }

    public void setYear(List<String> year) {
        this.year = year;
    }

    public int getSubQuery() {
        return subQuery;
    }

    public void setSubQuery(int subQuery) {
        this.subQuery = subQuery;
    }
    
}

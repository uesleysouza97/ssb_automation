package QueriesDAO;

import Queries.Query3;
import Results.ResultQuery3;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Query3DAO {

    Connection conn = null;
    Query3 query = null;
    PreparedStatement ps = null;
    List<Double> revenue;
    List<String> customerNation, supplierNation, year, supplierCity, customerCity;
    ResultQuery3 result = null;

    public Query3DAO(Connection conn, Query3 query) {

        this.conn = conn;
        this.query = query;
        revenue = new ArrayList<>();
        year = new ArrayList<>();
        customerNation = new ArrayList<>();
        supplierNation = new ArrayList<>();
        supplierCity = new ArrayList<>();
        customerCity = new ArrayList<>();

    }

    public void execute() {

        String sql = null;
        switch (query.getSubQuery()) {
            case 1:
                sql = "select sum(l.revenue) as revenue, c.nation as c_nation,"
                        + "s.nation as s_nation, d.year "
                        + "from lineorder as l, date as d, customer as c, supplier as s "
                        + "where l.custkey = c.custkey "
                        + "and l.orderdate = d.datekey "
                        + "and l.suppkey = s.suppkey "
                        + "and c.region = '" + query.getcRegion()
                        + "' and s.region = '" + query.getsRegion()
                        + "' and d.year >= " + query.getYear()
                        + " and d.year <= " + query.getYear_1()
                        + " group by l.revenue, d.year, c.nation, s.nation "
                        + "order by d.year asc, l.revenue desc";
                break;
            case 2:
                sql = "select sum(l.revenue) as revenue, c.city as c_city,"
                        + "s.city as s_city, d.year "
                        + "from lineorder as l, date as d, customer as c, supplier as s "
                        + "where l.custkey = c.custkey "
                        + "and l.orderdate = d.datekey "
                        + "and l.suppkey = s.suppkey "
                        + "and c.nation = '" + query.getcNation()
                        + "' and s.nation = '" + query.getsNation()
                        + "' and d.year >= " + query.getYear()
                        + " and d.year <= " + query.getYear_1()
                        + " group by l.revenue, c.city, s.city, d.year"
                        + " order by d.year asc, l.revenue desc";
                break;
            case 3:
                sql = "select sum(l.revenue) as revenue, c.city as c_city,"
                        + "s.city as s_city, d.year"
                        + " from lineorder as l, date as d, customer as c, supplier as s"
                        + " where l.custkey = c.custkey"
                        + " and l.orderdate = d.datekey"
                        + " and l.suppkey = s.suppkey"
                        + " and (c.city = '" + query.getcCity()
                        + "' or c.city = '" + query.getcCity_1()
                        + "') and (s.city = '" + query.getsCity()
                        + "' or s.city = '" + query.getsCity_1()
                        + "') and d.year >= " + query.getYear()
                        + " and d.year <= " + query.getYear_1()
                        + " group by l.revenue, c.city, s.city, d.year"
                        + " order by d.year asc, l.revenue desc";
                break;
            case 4:
                sql = "select sum(l.revenue) as revenue, c.city as c_city,"
                        + "s.city as s_city, d.year"
                        + " from lineorder as l, date as d, customer as c, supplier as s"
                        + " where l.custkey = c.custkey"
                        + " and l.orderdate = d.datekey"
                        + " and l.suppkey = s.suppkey"
                        + " and (c.city = '" + query.getcCity()
                        + "' or c.city = '" + query.getcCity_1()
                        + "') and (s.city = '" + query.getsCity()
                        + "' or s.city = '" + query.getsCity_1()
                        + "') and d.yearmonth = '" + query.getYearMonth()
                        + "' group by l.revenue, c.city, s.city, d.year"
                        + " order by d.year asc, l.revenue desc";
                break;
        }
        System.out.println("QUERY: " + sql);
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                revenue.add(rs.getDouble("revenue"));
                year.add(rs.getString("year"));
                if (query.getSubQuery() == 1) {
                    customerNation.add(rs.getString("c_nation"));
                    supplierNation.add(rs.getString("s_nation"));
                } else {
                    customerCity.add(rs.getString("c_city"));
                    supplierCity.add(rs.getString("s_city"));
                }
            }
            result = new ResultQuery3(revenue, customerNation, supplierNation, customerCity, supplierCity, year, query.getSubQuery());

        } catch (SQLException ex) {
            Logger.getLogger(Query2DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ResultQuery3 getResult() {
        return result;
    }
    
}

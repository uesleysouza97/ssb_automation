package QueriesDAO;

import Queries.Query2;
import Results.ResultQuery2;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Query2DAO {

    Connection conn = null;
    Query2 query = null;
    PreparedStatement ps = null;
    List<Double> revenue;
    List<String> year, brand;
    ResultQuery2 result = null;
    
    public Query2DAO(Connection conn, Query2 query) {

        this.conn = conn;
        this.query = query;
        revenue = new ArrayList<>();
        year = new ArrayList<>();
        brand = new ArrayList<>();
        
    }

    public void execute() {

        String sql = null;
        switch (query.getSubQuery()) {
            case 1:
                sql = "select sum(l.revenue) as revenue, d.year, p.brand1 "
                        + "from lineorder as l, date as d, part as p, supplier as s "
                        + "where l.orderdate = d.datekey "
                        + "and l.partkey = p.partkey "
                        + "and l.suppkey = s.suppkey "
                        + "and p.category = '" + query.getCategory()
                        + "' and s.region = '" + query.getRegion()
                        + "' group by d.year, p.brand1 order by d.year, p.brand1";
                break;
            case 2:
                sql = "select sum(l.revenue) as revenue, d.year, p.brand1 "
                        + "from lineorder as l, date as d, part as p, supplier as s "
                        + "where l.orderdate = d.datekey "
                        + "and l.partkey = p.partkey "
                        + "and l.suppkey = s.suppkey "
                        + "and p.brand1 between '" + query.getBrand1() + " and '"
                        + query.getBrand1_1() + "' "
                        + " and s.region = '" + query.getRegion()
                        + "' group by d.year, p.brand1 order by d.year, p.brand1";
                break;
            case 3:
                sql = "select sum(l.revenue) as revenue, d.year, p.brand1 "
                        + "from lineorder as l, date as d, part as p, supplier as s "
                        + "where l.orderdate = d.datekey "
                        + "and l.partkey = p.partkey "
                        + "and l.suppkey = s.suppkey "
                        + "and p.brand1 = '" + query.getBrand1() + "' "
                        + " and s.region = '" + query.getRegion()
                        + "' group by d.year, p.brand1 order by d.year, p.brand1";
                break;
        }
        System.out.println("QUERY: "+sql);
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                revenue.add(rs.getDouble("revenue"));
                year.add(rs.getString("year"));
                brand.add(rs.getString("brand1"));
            }
            result = new ResultQuery2(revenue, year, brand, query.getSubQuery());

        } catch (SQLException ex) {
            Logger.getLogger(Query2DAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ResultQuery2 getResult() {
        return result;
    }

}

package QueriesDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import Queries.Query1;
import Results.ResultQuery1;

public class Query1DAO {
    
    Connection conn = null;
    Query1 query = null;
    PreparedStatement ps = null;
    
    public Query1DAO(Connection conn, Query1 query) {
        
        this.conn = conn;
        this.query = query;
        
    }
    
    public ResultQuery1 execute() {
        
        String sql = "select sum(extendedprice*discount) as revenue "+
                "from lineorder as l, date as d "+
                "where l.orderdate = d.datekey "+
                "and d.year = " + query.getYear()+
                " and l.discount between " + query.getDiscount() + " - 1 " +
                "and " + query.getDiscount() + " + 1 and l.quantity < " + query.getQuantity();
        System.out.println("QUERY: "+sql);
        try {
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            Double result = null;
            while(rs.next()){
                result = rs.getDouble("revenue");
            }
            
            return new ResultQuery1(result, query);
           
        } catch (SQLException ex) {
            Logger.getLogger(Query1DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
       
    }
    
}
